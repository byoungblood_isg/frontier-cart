// Defining requirements
var gulp         = require( 'gulp' );
var sass         = require( 'gulp-sass' );
var watch        = require( 'gulp-watch' );
var rename       = require( 'gulp-rename' );
var concat       = require( 'gulp-concat' );
var uglify       = require( 'gulp-uglify' );
var browserSync  = require( 'browser-sync' ).create();
var autoprefixer = require( 'gulp-autoprefixer' );
var cleanCSS     = require( 'gulp-clean-css' );
var sourcemaps   = require( 'gulp-sourcemaps' );
var notify       = require( 'gulp-notify' );
var svgstore     = require( 'gulp-svgstore' );
var cheerio      = require( 'gulp-cheerio' );
var fs           = require('fs');

// Site Variables
var websiteURL = 'http://local.frontiercart.com';
var SSL = false;

/**
 * [handleErrors If Sass error]
 * @return [Don't Kill Watch]
 */
function handleErrors(err) {
	var args = Array.prototype.slice.call( arguments );

	// Ignore "untitled folder ENOENT" error (Gulp Watch Issue)
	if ( err.toString().indexOf('ENOENT') >= 0 ) {
		// Keep gulp from hanging on this task
		this.emit( 'end' );
	} else {
		// Send error to notification center with gulp-notify
		notify.onError({
			title: 'Compile Error',
			message: '<%= error %>'
		}).apply( this, args );

		// Keep gulp from hanging on this task
		this.emit( 'end' );
	}
}

// BrowserSync
gulp.task( 'browserSync' , function () {

	browserSync.init({
		proxy: websiteURL,
		https: SSL
	});

	// Reload PHP files
	gulp.watch( '**/*.php' )
		.on( 'error', handleErrors )
		.on( 'change', browserSync.reload );
});

// Compiles SCSS files in CSS
gulp.task( 'sass', function() {
	gulp.src( 'sass/**/*.scss' )
		.pipe( sourcemaps.init() )
		.pipe( sass().on( 'error', handleErrors ) )
		.pipe( sourcemaps.write() )
		.pipe( gulp.dest( './css' ) )
		.pipe( browserSync.stream() );
});

// Build CSS
gulp.task( 'buildcss', function() {
	gulp.src( 'sass/**/*.scss' )
		.pipe( sourcemaps.init() )
		.pipe( sass().on( 'error', handleErrors ) )
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
            cascade: false
		}))
		.pipe(cleanCSS(
			{
				compatibility: 'ie10',
				level: 2
			}
		))
		.pipe( sourcemaps.write() )
		.pipe( gulp.dest( './css' ) )
		.pipe( browserSync.stream() );
});

// Clean CSS (Production only)
gulp.task('cleancss', function() {
	return gulp.src('./css/*.css') // much faster
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
            cascade: false
		}))
		.pipe(cleanCSS(
			{
				compatibility: 'ie10',
				level: 2
			}
		))
		.pipe(gulp.dest('./css'));
});

// SVG Task
gulp.task( 'svgstore', function() {
	return gulp.src(['svg/*.svg'])
	.pipe( rename( { prefix: 'icon-' } ) )
	.pipe( svgstore( { inlineSvg: true } ) )
	.pipe( cheerio({
		run: function( $ ) {
			$( '[fill]' ).removeAttr( 'fill' );
			$( 'svg' ).attr( 'style', 'display:none' ).attr( 'width', '0' ).attr( 'height', '0' );
		},
		parserOptions: { xmlMode: true }
	} ) )
	.pipe( rename({
		basename: 'svg-icons',
		extname: '.php'
	} ) )
	.pipe( gulp.dest( './' ) );
});

// Uglifies and concat all JS files into one
gulp.task('scripts', function() {

	var jsfiles = JSON.parse(fs.readFileSync('./js/scripts.json'));
	var scripts = jsfiles.scripts;

	return gulp.src(scripts)
	.pipe( concat('theme.min.js').on( 'error', handleErrors ) )
	.pipe( uglify().on( 'error', handleErrors ) )
	.pipe(gulp.dest('./js/'))
	.pipe( browserSync.stream() );
});

// Build Task (Same as default but with css optimized)
gulp.task('build', ['browserSync'], function(){
	gulp.watch('sass/**/*.scss', ['buildcss']).on( 'error', handleErrors );
	gulp.watch(['svg/*.svg'], ['svgstore']).on( 'error', handleErrors );
	gulp.watch(['js/src/*.js'], ['scripts']).on( 'error', handleErrors );
});

// Production Task
gulp.task('production', ['cleancss']);

// Watch Task
gulp.task('default', ['browserSync'], function(){
	gulp.watch('sass/**/*.scss', ['sass']).on( 'error', handleErrors );
	gulp.watch(['svg/*.svg'], ['svgstore']).on( 'error', handleErrors );
	gulp.watch(['js/src/*.js'], ['scripts']).on( 'error', handleErrors );
	gulp.watch(['js/scripts.json'], ['scripts']).on( 'error', handleErrors );
});
